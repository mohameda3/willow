"""empty message

Revision ID: be638f761984
Revises: 4a99f11ba66b
Create Date: 2019-03-26 00:21:28.474890

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'be638f761984'
down_revision = '4a99f11ba66b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('course',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('code', sa.String(length=15), nullable=True),
    sa.Column('term', sa.String(length=10), nullable=True),
    sa.Column('year', sa.String(length=4), nullable=True),
    sa.Column('division', sa.String(length=20), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('course')
    # ### end Alembic commands ###
