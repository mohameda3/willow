import os


class Profile(object):
    DEBUG = False
    TESTING = False
    JSON_SORT_KEYS = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DISABLE_SWAGGER = False

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://postgres:docker@localhost:5432/public')
    REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379')
    MAX_TIMEOUT = -1

    SERVER_PORT = os.environ.get('SERVER_PORT', 8080)
    HOST = os.environ.get('HOST', 'localhost:{}'.format(SERVER_PORT))


class ProductionProfile(Profile):
    DEVELOPMENT = False
    DEBUG = False
    DISABLE_SWAGGER = True
    DATA_PREFIX = None


class StagingProfile(Profile):
    DEVELOPMENT = False
    DEBUG = True
    DATA_PREFIX = 'CSC'


class DevelopmentProfile(Profile):
    DEVELOPMENT = True
    DEBUG = True
    TESTING = True
    DATA_PREFIX = 'CSC'
