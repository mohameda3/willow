from decimal import Decimal


def to_json(inst, cls):
    """
    Jsonify the sql alchemy query result.
    """
    convert = dict()
    d = dict()
    for c in cls.__table__.columns:
        # Skip sensitive information
        if c.primary_key or c.foreign_keys or c.key.endswith('vector'):
            continue
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return d


def set_none_if_na(field):
    if field == 'N/A':
        return None
    return field


def get_decimal(field):
    if field is None:
        return None
    return Decimal(field)


def calculate_average(list_values):
    weighted_sum = 0
    divisor = 0
    for tup_v in list_values:
        value, weight = tup_v
        if value is None or weight is None:
            continue
        weighted_sum += value*weight
        divisor += weight

    if divisor == 0:
        return weighted_sum, divisor
    return weighted_sum/divisor, divisor


def get_dept_from_code(code):
    return code[:3]
