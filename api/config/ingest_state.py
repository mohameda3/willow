from enum import Enum


class IngestState(Enum):
    READY = 1
    PENDING = 2


class IngestStateHandler:
    state = IngestState.READY

    def pending(self):
        self.state = IngestState.PENDING

    def ready(self):
        self.state = IngestState.READY

    def get_state(self):
        return self.state
