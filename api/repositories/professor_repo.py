from api.repositories.base import Base
from api.models.professor import Professor


class ProfessorRepo(Base):

    def clear_data(self) -> None:
        Professor.query.delete()
        self.db.session.commit()

    @staticmethod
    def get_by_kwargs(kwarg_dict) -> Professor:
        prof = Professor.query.filter_by(**kwarg_dict).first()
        return prof

    @staticmethod
    def get_all_by_kwargs(kwarg_dict) -> list:
        all_profs = Professor.query.filter_by(**kwarg_dict).all()
        if all_profs is None:
            return []
        return all_profs
