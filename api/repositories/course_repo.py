from api.repositories.base import Base
from api.models.course import Course


class CourseRepo(Base):

    def clear_data(self):
        Course.query.delete()
        self.db.session.commit()

    @staticmethod
    def get_by_kwargs(kwarg_dict) -> Course:
        course = Course.query.filter_by(**kwarg_dict).first()
        return course

    @staticmethod
    def get_all_by_kwargs(kwarg_dict) -> list:
        all_courses = Course.query.filter_by(**kwarg_dict).all()
        if all_courses is None:
            return []
        return all_courses

    @staticmethod
    def search(keyword) -> list:
        all_courses = Course.query.with_entities(Course.code, Course.title, Course.division, Course.level).search(keyword).\
            distinct(Course.code).all()
        if all_courses is None:
            return []
        return all_courses
