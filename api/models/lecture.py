from api import db
from api.utils import to_json


class Lecture(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
    prof_id = db.Column(db.Integer, db.ForeignKey('professor.id'))
    activity = db.Column(db.String(10))
    time = db.Column(db.ARRAY(db.String))
    size = db.Column(db.String(10))
    delivery = db.Column(db.String(10))

    @property
    def json(self):
        return to_json(self, self.__class__)
