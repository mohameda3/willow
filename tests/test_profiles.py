import os


def test_base_profile(app):
    app.config.from_object('api.profiles.Profile')

    assert not app.config.get('JSON_SORT_KEYS')
    assert not app.config.get('SQLALCHEMY_TRACK_MODIFICATIONS')
    assert not app.config.get('DISABLE_SWAGGER')
    assert app.config.get('MAX_TIMEOUT') == -1

    assert app.config.get('SQLALCHEMY_DATABASE_URI') == os.environ.get(
        'DATABASE_URL', 'postgresql://postgres:docker@localhost:5432/public')
    assert app.config.get('REDIS_URL') == os.environ.get('REDIS_URL', 'redis://localhost:6379')
    assert app.config.get('SERVER_PORT') == os.environ.get('SERVER_PORT', 8080)
    assert app.config.get('HOST') == os.environ.get('HOST', 'localhost:{}'.format(os.environ.get('SERVER_PORT', 8080)))


def test_development_profile(app):
    app.config.from_object('api.profiles.DevelopmentProfile')

    assert app.config.get('DEVELOPMENT')
    assert app.config.get('DEBUG')
    assert app.config.get('TESTING')
    assert not app.config.get('DISABLE_SWAGGER')
    assert app.config.get('DATA_PREFIX') == 'CSC'


def test_staging_profile(app):
    app.config.from_object('api.profiles.StagingProfile')

    assert not app.config.get('DEVELOPMENT')
    assert app.config.get('DEBUG')
    assert not app.config.get('TESTING')
    assert not app.config.get('DISABLE_SWAGGER')
    assert app.config.get('DATA_PREFIX') == 'CSC'


def test_production_profile(app):
    app.config.from_object('api.profiles.ProductionProfile')

    assert not app.config.get('DEVELOPMENT')
    assert not app.config.get('DEBUG')
    assert not app.config.get('TESTING')
    assert app.config.get('DISABLE_SWAGGER')
    assert not app.config.get('DATA_PREFIX') == 'CSC'
