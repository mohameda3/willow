from unittest import mock
from api.models import professor_evaluation
from api.repositories import professor_evaluation_repo


@mock.patch('api.repositories.professor_evaluation_repo.ProfessorEvaluation')
def test_professor_evaluation_get_by_kwargs_when_evaluation_matches(mockProfessorEvaluation):
    test_prof_eval = professor_evaluation.ProfessorEvaluation(name='James')
    filter_by_mock = mockProfessorEvaluation.query.filter_by.return_value
    filter_by_mock.all.return_value = [test_prof_eval]

    lectures = professor_evaluation_repo.ProfessorEvaluationRepo.get_all_by_kwargs({'name': 'James'})
    assert lectures == [test_prof_eval]


@mock.patch('api.repositories.professor_evaluation_repo.ProfessorEvaluation')
def test_professor_evaluation_get_by_kwargs_when_no_evaluation_matches(mockProfessorEvaluation):
    filter_by_mock = mockProfessorEvaluation.query.filter_by.return_value
    filter_by_mock.all.return_value = None

    lectures = professor_evaluation_repo.ProfessorEvaluationRepo.get_all_by_kwargs({'name': 'James'})
    assert lectures == []
