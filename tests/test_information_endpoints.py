import json


def test_ping(app):
    client = app.test_client()
    resp = client.get('/ping')
    data = json.loads(resp.data)

    assert resp.status_code == 200
    assert data.get('message') == 'pong'


def test_swagger(app):
    assert not app.config.get('DISABLE_SWAGGER')

    client = app.test_client()

    assert client.get('/apidocs/').status_code is 200
    assert client.get('/apispec_1.json').status_code is 200