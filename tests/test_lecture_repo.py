from unittest import mock
from api.models import lecture
from api.repositories import lecture_repo


@mock.patch('api.repositories.lecture_repo.Lecture')
def test_lecture_get_by_kwargs_when_lecture_matches(mockLecture):
    test_lecture = lecture.Lecture(delivery='ONLINE')
    filter_by_mock = mockLecture.query.filter_by.return_value
    filter_by_mock.all.return_value = [test_lecture]

    lectures = lecture_repo.LectureRepo.get_all_by_kwargs({'delivery': 'ONLINE'})
    assert lectures == [test_lecture]


@mock.patch('api.repositories.lecture_repo.Lecture')
def test_lecture_get_by_kwargs_when_no_lecture_matches(mockLecture):
    filter_by_mock = mockLecture.query.filter_by.return_value
    filter_by_mock.all.return_value = None

    lectures = lecture_repo.LectureRepo.get_all_by_kwargs({'delivery': 'ONLINE'})
    assert lectures == []
