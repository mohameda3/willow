from unittest import mock
from api.models import course
from api.repositories import course_repo


@mock.patch('api.repositories.course_repo.Course')
def test_course_get_all_by_kwargs_when_course_matches(mockCourse):
    test_course = course.Course(year='2019')
    filter_by_mock = mockCourse.query.filter_by.return_value
    filter_by_mock.all.return_value = [test_course]

    courses = course_repo.CourseRepo.get_all_by_kwargs({'year': '2019'})
    assert courses == [test_course]


@mock.patch('api.repositories.course_repo.Course')
def test_course_get_all_by_kwargs_when_no_course_matches(mockCourse):
    filter_by_mock = mockCourse.query.filter_by.return_value
    filter_by_mock.all.return_value = None

    courses = course_repo.CourseRepo.get_all_by_kwargs({'year': '2019'})
    assert courses == []


@mock.patch('api.repositories.course_repo.Course')
def test_course_search_when_keyword_matches_courses(mockCourse):
    test_courses = [('CSC108H1', '', '', ''), ('CSC148H1', '', '', ''), ('CSC165H1', '', '', '')]
    with_entities_mock = mockCourse.query.with_entities.return_value
    search_mock = with_entities_mock.search.return_value
    distinct_mock = search_mock.distinct.return_value
    distinct_mock.all.return_value = test_courses

    courses = course_repo.CourseRepo.search('CSC')
    assert courses == test_courses


@mock.patch('api.repositories.course_repo.Course')
def test_course_search_when_keyword_matches_no_courses(mockCourse):
    with_entities_mock = mockCourse.query.with_entities.return_value
    search_mock = with_entities_mock.search.return_value
    distinct_mock = search_mock.distinct.return_value
    distinct_mock.all.return_value = None

    courses = course_repo.CourseRepo.search('CSC')
    assert courses == []
