import pytest
from api.app import app as application


@pytest.fixture
def app():
    application.config.from_object('api.profiles.DevelopmentProfile')
    return application
